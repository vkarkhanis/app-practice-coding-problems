export const chapterCode = {
  '5.3.1': {
    content: `

   /*
    * Accessing Character in string
    */ 

    const exampleString = "Hello World";
    
    // New syntax -- []
    const charAt2 = exampleString[2];

    // Traditional syntax
    const charAt3 = exampleString.charAt(7);

   /*
    * Difference between [] and charAt
    */
    const charAt100UsingNewSyntax = exampleString[100]  ?? "undefined";
    const charAt100UsingcharAt = exampleString.charAt(100);

    /* 
    * Convert string to camel case
    */
    const str = "Good morning";

   /*
    * Approach 1:
    */
    const camelCased = str.split(" ").reduce((acc, eachWord, idx) => 
      idx === 0 ?
        acc + eachWord.toLowerCase() : 
        acc + eachWord[0].toUpperCase() + eachWord.substr(1)

      
    , "");

   /*
    * Approach 2:
    */
    const camelCasedUsingMap = str.split(" ").map((eachWord, idx) => 
      idx === 0 ? eachWord.toLowerCase() : eachWord[0].toUpperCase() + eachWord.substr(1)).join("");


    return JSON.stringify({
        charAt2, 
        charAt3, 
        charAt100UsingNewSyntax, 
        charAt100UsingcharAt,
        camelCased,
        camelCasedUsingMap
    }, null, 2);
    `
  },
  '5.3.2': {
    content: `// Find first non repeating character in a given string

    const exampleString = "Good Morning";
    const exampleStringLowerCase = exampleString.toLowerCase();

    // Expected result = d

    for(let eachChar of exampleString) {
        
        if (exampleStringLowerCase.indexOf(eachChar.toLowerCase()) === exampleStringLowerCase.lastIndexOf(eachChar.toLowerCase())) {

            return JSON.stringify({"First non repeating character": eachChar}, null, 2);
        }
    }
    `
  },
  '6.1': {
    content: `// Too many nested callbacks results into unreadable code 
    // This is known as callback hell
    
    const result = {};

    fetch("https://dog.ceo/api/breeds/image/random")

     //First Callback
    .then(res1 => res1.json())
    .then(res1 => {
      result.callback1 = res1;
      fetch("https://dog.ceo/api/breeds/image/random")

      //Second Callback
      .then(res2 => res2.json())
      .then(res2 => {
         result.callback2 = res2;
         fetch("https://dog.ceo/api/breeds/image/random")
         
         //Third Callback
         .then(res3 => res2) 
         .then(res3 => {
           result.callback3 = res3;
           console.log(result); 
         })
       })  
    });

    return JSON.stringify(result);
    `
  },
  '6.3': {
    content: `// Promise chaining is still messy
    
    const result = {};

    fetch("https://dog.ceo/api/breeds/image/random")

     //First Callback
    .then(res1 => res1.json())
    .then(res1 => {
      result.callback1 = res1;
      fetch("https://dog.ceo/api/breeds/image/random")

      //Second Callback
      .then(res2 => res2.json())
      .then(res2 => {
         result.callback2 = res2;
         fetch("https://dog.ceo/api/breeds/image/random")
         
         //Third Callback
         .then(res3 => res2) 
         .then(res3 => {
           result.callback3 = res3;
           console.log(result); 
         })
       })  
    });

    return JSON.stringify(result);
    `
  },
  '6.4': {
    content: `    // Use Async-Await for clean code
    
    const result = {firstResult: {}, secondResult: {}};

    const fetchData = async () => {
     return await ((await fetch("https://dog.ceo/api/breeds/image/random")).json());
    }

   // Inline function call -- use async-await to make clean calls instead of Promise chaining
   // Async-Await is syntactic sugar over Promise, so internally it is still Promise processing

   (async () => {

     const res1 = await fetchData();
     console.log("First call: ", res1);

     const res2 = await fetchData();
     console.log("Second call: ", res2);

     const res3 = await fetchData();
     console.log("Third call: ", res3);
   })();
   
    
   return "Please refer to console for results. Owing to async processing, the results cannot be synchronously returned !!!"
    `
  }
};
