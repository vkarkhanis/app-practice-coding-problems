/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components';
import './App.css';
import Divider from './components/split-view/Divider';
import LeftPanel from './components/split-view/LeftPanel';
import RightPanel from './components/split-view/RightPanel';
import SplitView from './components/split-view/SplitView';
import Topics from './components/topics/Topics';
import Tree, { TreeData } from './components/tree/Tree';
import { chapterCode } from './topicData';

const TreeContainer = styled.div`
  background-color: #f0eae1;
  font-size: 10px;
`;

const topicsData: TreeData = {
  label: 'Javascript',
  key: '0',
  subTopic: '0',
  childNodes: [
    {
      label: 'Datatypes',
      key: '5',
      subTopic: '5',
      childNodes: [
        {
          label: 'String',
          key: '5.3',
          subTopic: '5.3',
          childNodes: [
            { label: 'character operation', key: '5.3.1', subTopic: '5.3.1' },
            { label: 'indexOf / lastIndexOf', key: '5.3.2', subTopic: '5.3.2' },
            { label: 'bitwise NOT', key: '5.3.3', subTopic: '5.3.3' },
            {
              label: 'includes / startsWith / endsWith',
              key: '5.3.4',
              subTopic: '5.3.4'
            },
            {
              label: 'slice / substring / substr',
              key: '5.3.5',
              subTopic: '5.3.5'
            },
            { label: 'string comparison', key: '5.3.6', subTopic: '5.3.6' },
            { label: 'surrogate pairs', key: '5.3.7', subTopic: '5.3.7' }
          ]
        },
        {
          label: 'Arrays',
          key: '5.4',
          subTopic: '5.4',
          childNodes: [
            { label: '"at"', key: '5.4.1', subTopic: '5.4.1' },
            {
              label: 'pop / push / shift / unshift',
              key: '5.4.2',
              subTopic: '5.4.2'
            },
            { label: 'bitwise NOT', key: '5.4.3', subTopic: '5.4.3' },
            { label: 'loops', key: '5.4.4', subTopic: '5.4.4' },
            {
              label: 'splice / slice / concat',
              key: '5.4.5',
              subTopic: '5.4.5'
            },
            { label: 'iterate', key: '5.4.6', subTopic: '5.4.6' },
            {
              label: 'indexOf / lastIndexOf / includes',
              key: '5.4.7',
              subTopic: '5.4.7'
            },
            {
              label: 'find / findIndex / findLastIndex',
              key: '5.4.8',
              subTopic: '5.4.8'
            },
            {
              label: 'filter / map / sort / reverse',
              key: '5.4.9',
              subTopic: '5.4.9'
            },
            { label: 'split / join', key: '5.4.10', subTopic: '5.4.10' },
            {
              label: 'reduce / reduceRight / isArray',
              key: '5.4.11',
              subTopic: '5.4.11'
            }
          ]
        },
        { label: 'Iterables', key: '5.6', subTopic: '5.6' }
      ]
    },
    {
      label: 'Promise',
      key: '6',
      subTopic: '6',
      childNodes: [
        { label: 'Callback Hell', key: '6.1', subTopic: '6.1' },
        { label: 'Promise', key: '6.2', subTopic: '6.2' },
        {
          label: 'Promise chaining',
          key: '6.3',
          subTopic: '6.3'
        },
        {
          label: 'Async-Await',
          key: '6.4',
          subTopic: '6.4'
        }
      ]
    }
  ]
};

function App() {
  const [selectedNode, setSelectedNode] = React.useState<TreeData>();
  const onNodesSelect = (data: TreeData) => {
    setSelectedNode(data);
  };

  return (
    <>
      <SplitView>
        <LeftPanel>
          <TreeContainer>
            <Tree data={topicsData} onNodeSelect={onNodesSelect} />
          </TreeContainer>
        </LeftPanel>

        <Divider />
        <RightPanel>
          <Topics topic={selectedNode?.subTopic} topicData={chapterCode} />
        </RightPanel>
      </SplitView>
    </>
  );
}

export default App;
