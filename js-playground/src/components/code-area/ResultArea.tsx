import styled from 'styled-components';

const ResultAreaContainer = styled.textarea`
  background-color: #100f10;
  color: #fff;
  border: 1px solid #fff;
  border-radius: 3px;
  width: 99%;
  display: flex;
  justify-content: center;
  height: 150px;
  resize: none;
`;

const ResultArea: React.FC<{ result: string }> = ({ result }) => {
  return <ResultAreaContainer readOnly value={result} />;
};

export default ResultArea;
