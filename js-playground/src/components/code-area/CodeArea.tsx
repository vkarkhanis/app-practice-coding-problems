import {
  faEdit,
  faEraser,
  faPlay,
  faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';

const TextAreaContainer = styled.div`
  display: flex;
`;

const FontAwesomeIconContainer = styled(FontAwesomeIcon)`
  width: 15px;
  padding: 5px;
  border: 1px ridge #000;
`;

const Controls = styled.div`
  cursor: pointer;
  background-color: #a1c2c8;
  height: 10%;
`;

const TextArea = styled.textarea<{ readOnly: boolean }>`
  height: 400px;
  width: 500px;
  background-color: ${({ readOnly }) => (readOnly ? '#d2dfdf' : '#f3cfab')};
`;

const CodeArea: React.FC<{
  // eslint-disable-next-line no-unused-vars
  onCodeSubmit: (codeString: string) => void;
  value?: string;
  onErase?: () => void;
}> = ({ onCodeSubmit, value, onErase }) => {
  const [currCode, setCurrCode] = React.useState<string>();
  const [nonEditable, setNonEditable] = React.useState<boolean>(true);

  React.useEffect(() => {
    setCurrCode(value || '');
    onErase?.();
  }, [value]);

  return (
    <>
      <TextAreaContainer>
        <TextArea
          onChange={(e) => setCurrCode(e.target.value)}
          value={currCode}
          readOnly={nonEditable}
        />

        <Controls>
          <FontAwesomeIconContainer
            icon={faPlay}
            onClick={() =>
              onCodeSubmit(currCode ?? 'return console.log("No Code")')
            }
            title="Run Code"
          />
          <FontAwesomeIconContainer
            icon={nonEditable ? faEdit : faTimesCircle}
            onClick={() => setNonEditable(!nonEditable)}
            title="Edit Code"
          />
          <FontAwesomeIconContainer
            icon={faEraser}
            onClick={onErase}
            title="Erase Results"
          />
        </Controls>
      </TextAreaContainer>
    </>
  );
};

export default CodeArea;
