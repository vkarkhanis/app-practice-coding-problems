import React, { ChangeEvent } from 'react';
import styled from 'styled-components';

const LOWEST_FLOOR = -3;
const HIGHEST_FLOOR = 50;

const errorMap = new Map();
errorMap.set(
  1,
  `Incorrect floor entered. Please enter floor  between ${HIGHEST_FLOOR} and ${LOWEST_FLOOR}`
);
errorMap.set(2, 'Selected floor is same as current floor');

const ErrorText = styled.div`
  color: red;
  font-size: 10px;
  font-weight: bold;
  margin-top: 5px;
`;

const ErrorDisplay: React.FC<{ errors: number[] }> = ({ errors }) => {
  return (
    <>
      {errors.map((eachErrorKey: number, idx: number) => {
        return (
          <ErrorText key={idx}>
            <li>{errorMap.get(eachErrorKey)}</li>
          </ErrorText>
        );
      })}
    </>
  );
};

const EscalatorDisplay = () => {
  const [currentFloor, setCurrentFloor] = React.useState<number>(0);
  const [newFloor, setNewFloor] = React.useState<number>(0);
  const [error, setError] = React.useState<number[]>([]);

  const clearErrors = (errorNumber?: number) => {
    if (errorNumber !== undefined && errorNumber !== null) {
      setError(error.filter((e: number) => e !== errorNumber));
    } else {
      setError([]);
    }
  };

  React.useEffect(() => {
    clearErrors();
    if (newFloor === currentFloor) {
      console.error('Entered floor is same as current floor');
      setError((prevErrors: number[]) =>
        Array.from(new Set([...prevErrors, 2]))
      );
    } else {
      if (newFloor < LOWEST_FLOOR || newFloor > HIGHEST_FLOOR) {
        console.error(
          `Please enter a floor between ${HIGHEST_FLOOR} and ${LOWEST_FLOOR}`
        );
        setError((prevErrors: number[]) =>
          Array.from(new Set([...prevErrors, 1]))
        );
      }
    }
  }, [newFloor]);

  const move = (isMovingUp = true) => {
    if (error && error.length > 0) {
      return;
    }
    let currFlr = currentFloor ?? 0;
    const floorChangeInterval = setInterval(() => {
      isMovingUp ? currFlr++ : currFlr--;
      setCurrentFloor((prevValue = 0) =>
        isMovingUp ? prevValue + 1 : prevValue - 1
      );

      if (currFlr === newFloor) {
        clearInterval(floorChangeInterval);
      }
    }, 2000);
  };

  const onFloorSubmitted = () => {
    if (newFloor < currentFloor) {
      move(false);
    } else if (newFloor > currentFloor) {
      move();
    }
  };

  return (
    <>
      <input
        type="number"
        value={newFloor}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setNewFloor(Number(e.target.value))
        }
      />

      <button onClick={onFloorSubmitted} disabled={!!error.length}>
        Select Floor
      </button>
      <div>{currentFloor}</div>
      <ErrorDisplay errors={error} />
    </>
  );
};

export default EscalatorDisplay;
