/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components';
import CodeArea from '../code-area/CodeArea';
import ResultArea from '../code-area/ResultArea';

const ResultAreaContainer = styled.div`
  width: 500px;
  margin-top: 10px;
`;

const ResultHeader = styled.div`
  height: 30px;
  background-color: grey;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  color: black;
  font-weight: bold;
  display: flex;
  justify-content: center;
`;

export type Content = {
  content: string;
};
export type TopicData = Record<string, Content>;

const TopicCode: React.FC<{ code: string }> = ({ code }) => {
  const [codeSubmitted, setCodeSubmitted] = React.useState<string>(() => {
    return new Function(code)();
  });

  const onCodeSubmit = (codeSubmitted: string) => {
    const fn = new Function(codeSubmitted);
    setCodeSubmitted(fn());
  };

  const onErase = () => {
    setCodeSubmitted('');
  };

  return (
    <>
      <CodeArea onCodeSubmit={onCodeSubmit} value={code} onErase={onErase} />
      <ResultAreaContainer>
        <ResultHeader>Result</ResultHeader>
        <ResultArea result={codeSubmitted} />
      </ResultAreaContainer>
    </>
  );
};

const Topics: React.FC<{
  topic: string | undefined;
  topicData: TopicData;
}> = ({ topic, topicData }) => {
  const topicCode = topic
    ? topicData[topic as keyof typeof topicData]?.content
    : 'console.log("No Code")';

  return <TopicCode code={topicCode ?? 'const str = "No code found"'} />;
};

export default Topics;
