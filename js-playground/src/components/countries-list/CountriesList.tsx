import React from 'react';

const CountriesList = () => {
  const [country, setCountry] = React.useState<string>('');
  const [countriesList, setCountriesList] = React.useState<string[]>([]);

  const onCountryEntered = () => {
    setCountriesList((prevList: string[]) =>
      [...new Set([...prevList, country])].sort((e1: string, e2: string) =>
        e1.toLowerCase().localeCompare(e2.toLowerCase())
      )
    );
  };
  return (
    <>
      <input
        type="text"
        value={country}
        onChange={(e) => setCountry(e.target.value)}
      />
      <button onClick={onCountryEntered}>Submit</button>

      {countriesList.map((eachCountry: string) => (
        <div>{eachCountry}</div>
      ))}
    </>
  );
};

export default CountriesList;
