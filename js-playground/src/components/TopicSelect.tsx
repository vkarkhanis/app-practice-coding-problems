import Select from 'react-select';

type SelectOptions = { label: string; value: string };

const TopicOptions: Array<SelectOptions> = [
  { label: 'String', value: 'string' },
  { label: 'Array', value: 'array' }
];

const TopicSelect = () => {
  const handleChange = (option: SelectOptions | null) => {
    console.log(option);
  };

  return <Select options={TopicOptions} onChange={handleChange} />;
};

export default TopicSelect;
