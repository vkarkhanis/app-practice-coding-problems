import React from 'react';
import { TreeData } from './Tree';

type TreeContextValue = {
  // eslint-disable-next-line no-unused-vars
  onNodeSelect?: (data: TreeData) => void;
};

export const TreeContext = React.createContext<TreeContextValue>({});
