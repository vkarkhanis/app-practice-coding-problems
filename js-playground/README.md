## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Description

The project has a component which shows a nested tree structure. The component is used as LeftSection and shows topics. on clicking a topic, it shows an editor on which there would be code relevant to selected topic as an attempt to provide quick view of the topic. The editor opens readonly however it is editable using the "Edit" button on top right. Once in editable mode, the user can write any Javascript code and use the "Run" button on the top to execute the code in editor and show the result in the "Result Panel" below.

### `Please note: To show the results in Results Panel, the code on editor should return JSON.strigify({resultObject})`

To erase the results, "Erase" button on top right can be used
