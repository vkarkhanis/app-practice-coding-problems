package com.practice;

import java.util.*;
import java.util.stream.Collectors;

public class LogProcessing
{
    public static List<String> processLogs(List<String> logs, int maxSpan) {

        return logs.stream()

                /**
                 * Split each String in List<String> logs into String[] {"userId", "timeStamp", "action"}
                 * Then create instance of LogDetails for each log String
                 */
                .map(eachLog -> {
                    String[] splitLog = eachLog.split(" ");
                    return new LogDetails(Integer.parseInt(splitLog[0]), Long.parseLong(splitLog[1]), splitLog[2]);
                })

                /**
                 * Collect each instance of LogDetails in a Map<Integer, TreeSet<LogDetails>>
                 * The Key in the Map is userId and the value is TreeSet of LogDetails for each userId
                 * TreeSet is needed in the value to sort LogDetails on TimeStamp
                 * Assumption is sign-in always happens before sign-out
                 * In case if there is a sign-in after sign-out it is considered that user signed-out once and then signed-in again
                 */
                .collect(Collectors.toMap(LogDetails::getUserId, (eachLogDetail) -> {
                            Set<LogDetails> details = new TreeSet<>(Comparator.comparing(LogDetails::getTimeStamp));
                            details.add(eachLogDetail);
                            return details;
                },
                        (set1, set2) -> {
                            set1.addAll(set2);
                            return set1;
                        })
                )
                .entrySet().stream()

                /**
                 * Use the Map<Integer, Set<TreeSet>> and pass stream of entrySets for further processing
                 * Here each Set (which is grouped by userId) is iterated.
                 * If diff between sign-in and sign-out time is less than or equal to maxSpan, the userId is added to a List
                 * Since a list is returned is it necessary to use flatMap else map method will create List<List<Integer>>
                 * Also note, since flat map is used, the list instance returned has to returned as stream (userIds.stream()) since flatMap works on Streams
                 * Also note use of  --- long[] signInTime = {-1};
                 * This is done because signInTime is required to be used inside forEach and a variable needs to be final-like
                 * We could not use final variable as it had to be changed on every iteration, hence int Array of size 1 is used, which makes it final-like
                 *
                 */
                .flatMap((eachMapEntry) -> {
                    List<Integer> userIds = new ArrayList<>();
                    long[] signInTime = {-1};

                    eachMapEntry.getValue().forEach((eachLogDetail) -> {
                        if("sign-in".equalsIgnoreCase(eachLogDetail.getAction())) {
                            signInTime[0] = eachLogDetail.getTimeStamp();
                        }

                        if("sign-out".equalsIgnoreCase(eachLogDetail.getAction())) {
                            if (signInTime.length == 1 && signInTime[0] > -1 &&
                            eachLogDetail.getTimeStamp() - signInTime[0] <= maxSpan) {
                                userIds.add(eachLogDetail.getUserId());
                            }
                        }
                    });

                    return userIds.stream();
                })

                /**
                 * The above section returns List<Integer>
                 * Since we need to return numerically-sorted List converted to String, we use --
                 * sorted, then convert into String and collect in a List which is returned
                 */
                .sorted().map(eachUserId -> eachUserId.toString()).collect(Collectors.toList());


    }

    public static void main(String args[]) {
        LogProcessing processing = new LogProcessing();

        List<String> logDetails = Arrays.asList(new String[] {"30 99 sign-in",
                "30 105 sign-out",
                "12 100 sign-in",
                "20 80 sign-in",
                "12 120 sign-out",
                "20 101 sign-out",
                "21 110 sign-in"});

        int maxSpan = 20;

        List<String> userIds = processing.processLogs(logDetails, maxSpan);
        System.out.format("Users who have stayed logged in less than maxSpan of : %d ", maxSpan );
        System.out.println();
        System.out.format("%s ", userIds );
    }

    static class LogDetails {

        private int userId;
        private long timeStamp;
        private String action;

        public LogDetails(int userId, long timeStamp, String action) {
            this.userId = userId;
            this.timeStamp = timeStamp;
            this.action = action;
        }

        public int getUserId() {
            return userId;
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public String getAction() {
            return action;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LogDetails that = (LogDetails) o;
            return userId == that.userId && timeStamp == that.timeStamp && Objects.equals(action, that.action);
        }

        @Override
        public int hashCode() {
            return Objects.hash(userId, timeStamp, action);
        }
    }
}
