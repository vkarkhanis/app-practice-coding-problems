1. **Log Processing -- (Java file LogProcessing.java):**
   Application logs are used in analysis of interactions with an application and may be used to detect specific actions.
   A log file is provided as a String array where each entry is in the form "user_id timestamp action".
   Each of the values is separated by a space.

    1. Both user_id and timestamp consists only of digits are at-most 9 digits long and start with a non-zero digit.
    2. timestamp represents the time in seconds since the application was last launched.
    3. action will be either "sign-in" or "sign-out"

    Given a log with entries in no particular order return an array of Strings that denote user ids of users who signed out in
    maxSpan seconds or less after signing in
    
    **Example:**
    _n = 7
    logs = ["30 99 sign-in", "30 105 sign-out", "12 100 sign-in", "20 80 sign-in", "12 120 sign-out", "20 101 sign-out", "21 110 sign-in"]
    maxSpan = 20_
    
    _The users with ids 30 and 12 were not signed in for more than maxSpan = 20 seconds. 
    In sorted numerical order, the return array is [12, 30]_
    