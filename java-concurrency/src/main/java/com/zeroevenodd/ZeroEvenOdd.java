package com.zeroevenodd;

import java.util.concurrent.Semaphore;

public class ZeroEvenOdd {

    private int n;
    private Semaphore zeroSemaphore = new Semaphore(1);
    private Semaphore oddSemaphore = new Semaphore(0);
    private Semaphore evenSemaphore = new Semaphore(0);

    private PrintService printService;

    public ZeroEvenOdd(int n, PrintService printService) {
        this.n = n;
        this.printService = printService;
    }

    public void printZero() {

        for(int i = 1; i <= n; i++){

            try {

                zeroSemaphore.acquire();
                this.printService.printZeroMessage();
            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                if (i % 2 == 0) {
                    evenSemaphore.release();
                } else {
                    oddSemaphore.release();
                }
            }
        }

    }

    public void printOddNumber() {

        for(int i = 1; i <= n; i += 2){

            try {
                oddSemaphore.acquire();
                printService.printNumberMessage(i);
            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                zeroSemaphore.release();
            }
        }

    }

    public void printEvenNumber() {

        for(int i = 2; i <= n; i+=2){

            try {
                evenSemaphore.acquire();
                printService.printNumberMessage(i);
            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                zeroSemaphore.release();
            }
        }

    }
}
