package com.zeroevenodd;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ZeroEvenOddEntryPoint {

    public static void main(String args[]) {

        ZeroEvenOdd zeroEvenOdd = new ZeroEvenOdd(10, new PrintService());
        ExecutorService service = Executors.newFixedThreadPool(3);

        service.execute(() -> zeroEvenOdd.printZero());
        service.execute(() -> zeroEvenOdd.printEvenNumber());
        service.execute(() -> zeroEvenOdd.printOddNumber());

        service.shutdown();

    }
}
