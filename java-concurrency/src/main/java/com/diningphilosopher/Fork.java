package com.diningphilosopher;

import java.util.concurrent.locks.Lock;
public class Fork {

    private Lock lock;
    private int id;

    public Fork(Lock lock, int id) {
        this.id = id;
        this.lock = lock;
    }

    public void pickUpFork() {
        lock.lock();
    }

    public void putDownFork() {
        lock.unlock();
    }

    public int getId() {
        return this.id;
    }
}
