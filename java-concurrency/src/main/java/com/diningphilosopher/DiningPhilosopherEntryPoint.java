package com.diningphilosopher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class DiningPhilosopherEntryPoint {

    public static void main(String args[]) throws InterruptedException {

        List<Philosopher> philosophers = new ArrayList<>();
        List<Fork> forks = Arrays.asList(
                new Fork[]{new Fork(new ReentrantLock(), 0),
                new Fork(new ReentrantLock(), 1), new Fork(new ReentrantLock(), 2),
                        new Fork(new ReentrantLock(), 3), new Fork(new ReentrantLock(), 4)});

        for (int i = 0; i < 5; i++) {
            Fork leftFork = forks.get(i);
            Fork rightFork = forks.get((i + 1) % forks.size());

            philosophers.add(new Philosopher(i,"Philosopher " + i, leftFork, rightFork));
        }

        ExecutorService service = Executors.newFixedThreadPool(5);

        try {
            while(true) {
                service.invokeAll(philosophers);
            }
        } catch (InterruptedException e) {
           throw e;
        }
    }

}
