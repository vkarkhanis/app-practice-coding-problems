package com.diningphilosopher;

import java.util.concurrent.Callable;

public class Philosopher implements Callable<String> {

    private String name;
    private Fork leftFork;
    private Fork rightFork;
    private int id;

    public Philosopher(int id, String name, Fork leftFork, Fork rightFork) {
        this.id = id;
        this.name = name;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    public String call() throws Exception {

        doAction(Action.THINK);

        try {

            /**
             * This is done to avoid deadlock.
             * If this is not done and all Philosophers can pick up left spoon and deadlock can occur
             */
            if(this.id % 2 == 0) {
                doAction(Action.PICK_LEFT_FORK);
                doAction(Action.PICK_RIGHT_FORK);

            } else {
                doAction(Action.PICK_RIGHT_FORK);
                doAction(Action.PICK_LEFT_FORK);
            }


            doAction(Action.EAT);

            if(this.id % 2 == 0) {
                doAction(Action.PUT_DOWN_LEFT_FORK);
                doAction(Action.PUT_DOWN_RIGHT_FORK);
            } else {
                doAction(Action.PUT_DOWN_RIGHT_FORK);
                doAction(Action.PUT_DOWN_LEFT_FORK);
            }

        } catch (Exception e) {
            throw e;
        }

        return "SUCCESS";

    }

    private void doAction(Action action) throws Exception {

        switch(action) {

            case THINK:
                System.out.println(this.name + " is thinking");
                break;

            case EAT:
                System.out.println(this.name + " is eating");
                break;

            case PICK_LEFT_FORK:
                this.leftFork.pickUpFork();
                System.out.println(this.name + " has picked up left fork with id : " + leftFork.getId());
                break;

            case PICK_RIGHT_FORK:
                this.rightFork.pickUpFork();
                System.out.println(this.name + " has picked up right fork with id : " + rightFork.getId());
                break;

            case PUT_DOWN_LEFT_FORK:
                this.leftFork.putDownFork();
                System.out.println(this.name + " has put down left fork with id : " + leftFork.getId());
                break;

            case PUT_DOWN_RIGHT_FORK:
                this.rightFork.putDownFork();
                System.out.println(this.name + " has put down right fork with id : " + rightFork.getId());
                break;

            default:
                throw new Exception("Incorrect Action");
        }

        try {
            Thread.sleep(action.getMs());
        } catch (InterruptedException e) {
           throw e;
        }

    }

    private enum Action {
        THINK(3000), EAT(2000), PICK_LEFT_FORK(1000), PICK_RIGHT_FORK(1000), PUT_DOWN_LEFT_FORK(1000), PUT_DOWN_RIGHT_FORK(1000);

        Action(int ms) {
            this.ms = ms;
        }

        public int getMs() {
            return ms;
        }

        private int ms;
    }
}
