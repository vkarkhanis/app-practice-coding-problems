package com.foobarprint;

public class PrintingService {

    public void printFooMessage() {
        System.out.print("Foo");
    }

    public void printBarMessage() {
        System.out.print("Bar");
    }
}
