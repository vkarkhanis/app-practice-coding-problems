package com.foobarprint;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PrintFooBarEntryPoint {

    public static void main(String args[]) {

        ExecutorService service = Executors.newFixedThreadPool(2);
        PrintFooBar fooBar = new PrintFooBar(4, new PrintingService());

        service.submit(() -> {
            fooBar.printFoo();
        });

        service.submit(() -> {
            fooBar.printBar();
        });

        service.shutdown();
    }
}
