package com.foobarprint;

public class PrintFooBar{

    private int n;
    private boolean printFooFlag = false;

    private PrintingService printingService;

    public PrintFooBar(int n, PrintingService printingService) {
        this.n = n;
        this.printingService = printingService;
    }

    public synchronized void printFoo(){

        for(int i = 0; i < this.n; i++) {

            while (this.printFooFlag) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            this.printFooFlag = true;
            this.printingService.printFooMessage();
            notifyAll();

        }
    }

    public synchronized void printBar(){

        for(int i = 0; i < this.n; i++) {
            while(!this.printFooFlag) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            this.printFooFlag = false;
            this.printingService.printBarMessage();
            notifyAll();
        }
    }
}
