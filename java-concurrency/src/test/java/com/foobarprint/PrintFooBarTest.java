package com.foobarprint;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PrintFooBarTest {

    @Mock
    private PrintingService printingService;

    @Test
    public void printFooBarAlternativelyTest() {

        ExecutorService service = Executors.newFixedThreadPool(2);
        PrintFooBar fooBar = new PrintFooBar(4, printingService);

        service.submit(() -> fooBar.printFoo());
        service.submit(() -> fooBar.printBar());

        verify(printingService, times(4)).printFooMessage();
        verify(printingService, times(4)).printBarMessage();

        service.shutdown();

    }

}
