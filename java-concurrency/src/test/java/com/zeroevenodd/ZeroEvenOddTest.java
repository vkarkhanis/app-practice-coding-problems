package com.zeroevenodd;

import com.zeroevenodd.PrintService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ZeroEvenOddTest {

    @Mock
    PrintService printService;

    @Test
    public void alertnateZeroNumberPrintTest() {

        ZeroEvenOdd zeroEvenOdd = new ZeroEvenOdd(4, printService);
        ExecutorService service = Executors.newFixedThreadPool(3);

        service.submit(() -> zeroEvenOdd.printZero());
        service.submit(() -> zeroEvenOdd.printOddNumber());
        service.submit(() -> zeroEvenOdd.printEvenNumber());
        service.shutdown();

        verify(printService, times(1)).printZeroMessage();
        verify(printService, times(1)).printNumberMessage(1);

    }
}
