package com.diningphilosopher;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PhilosopherTest {

    @Mock
    Fork leftFork;

    @Mock
    Fork rightFork;

    @Test
    public void callTest() throws Exception {

        Philosopher testPhilosopher = new Philosopher(1,"Philosopher 1", leftFork, rightFork);
        testPhilosopher.call();
        verify(leftFork, times(1)).pickUpFork();
        verify(rightFork, times(1)).pickUpFork();
    }

}
