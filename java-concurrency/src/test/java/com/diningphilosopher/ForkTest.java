package com.diningphilosopher;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.locks.Lock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ForkTest {

    @Mock
    private Lock lock;

    @Test
    public void pickUpForkTest() {

        Fork fork = new Fork(lock, 1);
        fork.pickUpFork();

        verify(lock, times(1)).lock();
    }

    @Test
    public void putDownForkTest() {

        Fork fork = new Fork(lock, 1);
        fork.putDownFork();

        verify(lock, times(1)).unlock();
    }

    @Test
    public void getIdTest() {

        Fork fork = new Fork(lock, 1);
        assertEquals(1, fork.getId());
    }
}
